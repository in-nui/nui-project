**NUI-Project** is an attempt to create an **immersive virtual experience** with **natural user interaction** using a **head-mounted display** and **body motion tracking**.

The project was done as part of a lecture in computer science master at the [University of Applied Sciences Ravensburg-Weingarten](http://www.hs-weingarten.de/).